import Vue from 'vue'
//import Dashboard from './components/pages/Dashboard.vue'
import Patron from './components/pages/Patron.vue'
//import Books from './components/pages/Books.vue'
//import Settings from './components/pages/Settings.vue'


import './assets/css/design.css'
import './assets/css/responsive.css'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
new Vue({
  //render: function (h) { return h(Dashboard) },
  render: function (h) { return h(Patron) },
  //render: function (h) { return h(Books) },
  //render: function (h) { return h(Settings) },
}).$mount('#app')
